from nltk.corpus import sentiwordnet as swn
from nltk import tokenize
import nltk
import re

def review_score(review):
	review = re.sub('([.,!?()])', r' \1 ', review)
	sents = tokenize.sent_tokenize(review)
	score = [0.0, 0.0, 0.0]
	no_of_sents = 0
	for sentences in sents:
		if len(sentences.strip()) <= 0:
			continue
		temp_score = sentence_score(sentences.strip())
		score[:] = [sum(i) for i in zip(score,temp_score)]
		no_of_sents = no_of_sents + 1
	if(no_of_sents > 0):
		score[:] = [x/no_of_sents for x in score]
	print score

def sentence_score(sentence):
	text = nltk.word_tokenize(sentence)
	score = [0.0, 0.0, 0.0]
	no_of_words = 0
	for words in nltk.pos_tag(text):
		temp_pos = words[1]
		if temp_pos in ["JJ","JJR","JJS"]:
			pos = "a"
		elif temp_pos in ["NN","NNS","NNP","NNPS"]:
			pos = "n"
		elif temp_pos in ["RB","RBR","RBS"]:
			pos = "r"
		elif temp_pos in ["VB","VBD","VBG","VBN","VBP","VBZ"]:
			pos = "v"
		elif temp_pos in [".",",",";",":"]:
			continue
		else:
			pos = "k"
		temp_score = word_score(words[0],pos)
		if(temp_score[2] >= 0.5):
			continue
		score[:] = [sum(i) for i in zip(score,temp_score)]
		no_of_words = no_of_words + 1
	if(no_of_words > 0):
		score[:] = [x/no_of_words for x in score]
	return score

def word_score(word,tag):
	if tag == "k":
		all_words = swn.senti_synsets(word)
	else:
		all_words = swn.senti_synsets(word,tag)
	score = [0.0, 0.0, 0.0]
	for words in all_words:
		score[0] = score[0] + words.pos_score()
		score[1] = score[1] + words.neg_score()
		score[2] = score[2] + words.obj_score()
	if(len(all_words) > 0):
		score[0] = score[0] / len(all_words)
		score[1] = score[1] / len(all_words)
		score[2] = score[2] / len(all_words)
	return score

review_score(raw_input("Enter Paragraph:\n").lower())